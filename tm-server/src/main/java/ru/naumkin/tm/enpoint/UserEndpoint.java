package ru.naumkin.tm.enpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.naumkin.tm.api.endpoint.IUserEndpoint;
import ru.naumkin.tm.api.service.ISessionService;
import ru.naumkin.tm.api.service.IUserService;
import ru.naumkin.tm.entity.Session;
import ru.naumkin.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.naumkin.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull private IUserService userService;

    public UserEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final IUserService userService) {
        super(sessionService);
        this.userService = userService;
    }

    @NotNull
    @Override
    @WebMethod
    public List<User> findAllUsers() throws Exception {
        return new LinkedList<>(userService.findAll());
    }

    @Nullable
    @Override
    @WebMethod
    public User findOneUser(
            @Nullable final String name
    ) throws Exception {
        return userService.findOne(name);
    }

    @Override
    @WebMethod
    public void persistUser(
            @NotNull final User user
    ) throws Exception {
        userService.persist(user);
    }

    @Override
    @WebMethod
    public void mergeUser(
            @NotNull final String sessionToken,
            @NotNull final User user
    ) throws Exception {
        validate(sessionToken);
        userService.merge(user);
    }

    @Override
    @WebMethod
    public void removeUser(
            @NotNull final String sessionToken,
            @NotNull final User user
    ) throws Exception {
        validate(sessionToken);
        userService.remove(user);
    }

    @Override
    @WebMethod
    public void removeAllUser(@NotNull final String sessionToken) throws Exception {
        validate(sessionToken);
        userService.removeAll();
    }

    @Override
    @WebMethod
    public boolean isRoleAdmin(
            @NotNull final String sessionToken,
            @NotNull final String id
    ) throws Exception {
        validate(sessionToken);
        return userService.isRoleAdmin(id);
    }

}
